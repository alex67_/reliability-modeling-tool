﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Collections.ObjectModel;
namespace Reliability_Modeling_Application
{
    enum TypeOfControl
    {
        unDistributed,unFunction
    }
    public enum TypeOfElement
    {
        Folder,Element,GeneralUnit
    }
    class Component : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void onPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, e);
            }
        }
        public bool IsGeneral;
        private ObservableCollection<string> modes;
        public ObservableCollection<string> Modes
        {
            get { return modes; }
            set { modes = value; onPropertyChanged(new PropertyChangedEventArgs("Modes")); }
        }
        private ObservableCollection<string> states;
        public ObservableCollection<string> States
        {
            get { return states; }
            set { states = value; onPropertyChanged(new PropertyChangedEventArgs("States")); }
        }
        private ObservableCollection<string> statesWithoutFail;
        public ObservableCollection<string> StatesWithoutFail
        {
            get { return statesWithoutFail; }
            set { statesWithoutFail = value; onPropertyChanged(new PropertyChangedEventArgs("StatesWithoutFail")); }
        }
        public string initialState;
        public string initialMode;
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; onPropertyChanged(new PropertyChangedEventArgs("Name")); }
        }
        public string Id;
        public DataTable table_distributions;
        public bool IsWrapped;
        public TypeOfControl control;
        public DataTable table_statesChanges;
        public Component(ObservableCollection<string> m, ObservableCollection<string> s,bool isGeneral=false, string Name_=" New component",bool isWrapped=false)
        {
            Modes = m;
            States = s;
            IsGeneral = isGeneral;
            Name = Name_;
            IsWrapped = isWrapped;
            if (m.Count>0) initialMode = m[0];
            if (s.Count > 0) initialState = m[0];
            StatesWithoutFail = new ObservableCollection<string>(from st in States where st!="Fail" select st);
            table_distributions = new DataTable();
            table_statesChanges = new DataTable();
            initTables();
            States.CollectionChanged += States_CollectionChanged;
            Modes.CollectionChanged += Modes_CollectionChanged;
        }
        private void initTables()
        {
            foreach (string item in Modes)
            {
                table_distributions.Columns.Add(item, typeof(double));
                table_statesChanges.Columns.Add(item, typeof(string));
            }
            for (int i = 0; i <  StatesWithoutFail.Count; i++)
            {
                table_distributions.Rows.Add(table_distributions.NewRow());
                table_statesChanges.Rows.Add(table_statesChanges.NewRow());
            }
        }
        private void Modes_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                foreach (string item in e.NewItems)
                {
                    table_distributions.Columns.Add(item, typeof(double));
                    table_statesChanges.Columns.Add(item, typeof(string));
                }
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                for (int i=0; i< e.OldItems.Count;i++)
                {
                    table_distributions.Columns.RemoveAt(e.OldStartingIndex+i);
                    table_statesChanges.Columns.RemoveAt(e.OldStartingIndex + i);
                }
            }
        }

        private void States_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                foreach (string item in e.NewItems)
                {
                    if(item!= "Fail")
                    {
                        StatesWithoutFail.Add(item);
                        table_distributions.Rows.Add(table_distributions.NewRow());
                        table_statesChanges.Rows.Add(table_statesChanges.NewRow());
                    }
  
                }
            if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {
                for (int i = 0; i < e.OldItems.Count; i++)
                {
                    StatesWithoutFail.Remove(e.OldItems[i] as string);
                    if (e.OldItems[i] as string != "Fail")
                    {
                        table_distributions.Rows.RemoveAt(e.OldStartingIndex + i);
                        table_statesChanges.Rows.RemoveAt(e.OldStartingIndex + i);
                    }
                }
            }
        }
    }
}

