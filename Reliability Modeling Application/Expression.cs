﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reliability_Modeling_Application
{
    enum LogicalOperator
    {
        OR,NOT,AND,NOR,NAND
    }
    interface ICondition
    {
        string generateCondition();
    }

    class Expression:ICondition
    {
        private ICondition leftOperand;
        public ICondition LeftOperand
        {
            set { leftOperand = value; }
            get { return leftOperand; }
        }

        private ICondition rightOperand;
        public ICondition RightOperand
        {
            set { rightOperand = value; }
            get { return rightOperand; }
        }
        private LogicalOperator operator_;
        public LogicalOperator Operator_
        {
            set { operator_ = value; }
            get { return operator_; }
        }
        public string generateCondition()
        {
            return "";
        }
    }
    class SingleCondition:ICondition
    {
        public string generateCondition()
        {
            return "";
        }
        public Component component;
        public string previousStateOrMode;
        public string nextStateOrMode;
        public string action;
    }
}
