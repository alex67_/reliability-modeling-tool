﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Reliability_Modeling_Application
{
    class ConverterForRadioButton : System.Windows.Data.IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            Component comp = (Component)values[0];
            string state = (string)values[1];
            if (values.Length == 3)
            {
                if (state == comp.initialMode)
                    return true;
                else return false;
            }
            else
            {
            if (state == comp.initialState)
                return true;
            else return false;
            }

        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
