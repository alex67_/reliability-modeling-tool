﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using AvalonDock;
using System.Collections.ObjectModel;
namespace Reliability_Modeling_Application
{
    public partial class MainWindow : Window
    {
        List<string> actions;
        Component current_component;
        ObservableCollection<Component> components;
        ICondition current_condition;
        List<ICondition> conditions;
        public MainWindow()
        {
            InitializeComponent();
            components = new ObservableCollection<Component>();
            addGeneralUnit();
            actions = new List<string>()
            {
                "Выберите действие",
                "Изменить состояние",
                "Изменить режим",
                "Состояние",
                "Режим"
            };
            components.Add(new Component(new ObservableCollection<string>(),new ObservableCollection<string>(),false,"Выражение",true));
            ((ComboBox)(((Grid)tree1.Items[1]).Children[5])).ItemsSource = actions;
            ((ComboBox)(((Grid)tree1.Items[1]).Children[5])).SelectedIndex = 0;
            ((ComboBox)(((Grid)tree1.Items[1]).Children[3])).SelectedIndex = -1;
            ((ComboBox)(((Grid)tree1.Items[1]).Children[3])).ItemsSource = components;
            ((ComboBox)(((Grid)tree1.Items[1]).Children[3])).DisplayMemberPath = "Name";
            
        }
        
        //Menu for component
        private void MItem_Rename_Click(object sender, RoutedEventArgs e)
        {
            MenuItem obMenuItem = e.OriginalSource as MenuItem;
            ContextMenu menu = obMenuItem.Parent as ContextMenu;
            TreeViewItem item = menu.PlacementTarget as TreeViewItem;
            StackPanel pan = item.Header as StackPanel;
            
            TextBox txtBox = new TextBox();
            txtBox.BorderThickness = new Thickness(0);
            txtBox.Margin = new Thickness(3, 5, 5, 5);
            txtBox.Text = (string)((pan.Children[1] as Label).Content);
            txtBox.Tag = (pan.Children[1] as Label).Tag;
            pan.Children.RemoveAt(1);

            txtBox.SelectAll();
            txtBox.LostFocus += txtBox_lostFocus;
            txtBox.KeyDown += TxtBox_KeyDown;
            pan.Children.Add(txtBox);
        }
        private void MItem_Delete_Click(object sender, RoutedEventArgs e)
        {
            MenuItem obMenuItem = e.OriginalSource as MenuItem;
            ContextMenu menu = obMenuItem.Parent as ContextMenu;
            TreeViewItem treeItem = menu.PlacementTarget as TreeViewItem;

            if ((string)obMenuItem.Header == "Удалить")
            {
                if(treeItem.Tag!=null && treeItem.Tag is Component)
                    components.Remove(treeItem.Tag as Component);
                if (treeItem.Parent is TreeViewItem)
                    ((TreeViewItem)treeItem.Parent).Items.Remove(treeItem);
                else if (treeItem.Parent is TreeView)
                    ((TreeView)treeItem.Parent).Items.Remove(treeItem);
            }
        }
        //Handlers for tree of components
        private void TreeView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            TreeView tree = sender as TreeView;
            if(tree.SelectedItem!=null)
                ((TreeViewItem)tree.SelectedItem).IsSelected = false;
        }
        private void tree_comp_SelectedItemChanged(object sender,  RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)(sender as TreeView).SelectedItem ;
            if (item == null) return;
            if (item.Tag == null) return;
            if (item.Tag is TypeOfElement)
                return;
            else
                if (item.Tag is Component)
            {
                current_component = (Component)item.Tag;
            };
            setComponentInPropertiesTree();
            updateTablesInPropertiesTree(current_component);
        }
        private void addGeneralUnit()
        {
            TreeViewItem item = new TreeViewItem();
            StackPanel panel = new StackPanel();
            Image img = new Image();
            Label label = new Label();
            Component newComponent;
            panel.Orientation = Orientation.Horizontal;
            img.Source = (ImageSource)Resources["imageComponent"];

            img.Width = 16;
            img.Source = (ImageSource)this.FindResource("imageComponent");
            label.Content = "Устройство";
            panel.Children.Add(img);
            panel.Children.Add(label);

            item.Header = panel;
            newComponent = new Component(new ObservableCollection<string>(), new ObservableCollection<string>());
            newComponent.Name = "Устройство";
            newComponent.IsGeneral = true;
            label.Tag = newComponent;
            item.Tag = newComponent;
            components.Add(newComponent);
            item.DataContext = ((Component)item.Tag);
            tree_comp.Items.Add(item);

            item.Focus();
            panel.Focus();
        }
        private void setComponentInPropertiesTree()
        {
            treeProperties.DataContext = current_component;
            listBoxOfStates.ItemsSource = current_component.States;
            listBoxOfModes.ItemsSource = current_component.Modes;
            listBox_choose_first_state.DataContext = current_component;
            listBox_choose_first_state.ItemsSource = current_component.StatesWithoutFail;
            listBox_choose_first_mode.ItemsSource = current_component.Modes;
            radioB_controlUnDis.Tag = TypeOfControl.unDistributed;
            radioB_controlUnFun.Tag = TypeOfControl.unFunction;
            switch (current_component.control)
            {
                case TypeOfControl.unDistributed:
                    radioB_controlUnDis.IsChecked = true;
                    break;
                case TypeOfControl.unFunction:
                    radioB_controlUnFun.IsChecked = true;
                    break;
                default:
                    break;
            }
}        
        //Handlers for the panel "Component properties"
        private void MItem_deleteState_Click(object sender, RoutedEventArgs e)
        {
            MenuItem obMenuItem = e.OriginalSource as MenuItem;
            ContextMenu menu = obMenuItem.Parent as ContextMenu;
            ListBox box = menu.PlacementTarget as ListBox;
            if ((string)obMenuItem.Header == "Удалить")
            {
                current_component.States.Remove((string)box.SelectedItem);
                updateTablesInPropertiesTree(current_component);
            }
        }
        private void MItem_deleteMode_Click(object sender, RoutedEventArgs e)
        {
            MenuItem obMenuItem = e.OriginalSource as MenuItem;
            ContextMenu menu = obMenuItem.Parent as ContextMenu;
            ListBox box = menu.PlacementTarget as ListBox;
            if ((string)obMenuItem.Header == "Удалить")
            {
                current_component.Modes.Remove((string)box.SelectedItem);
                updateTablesInPropertiesTree(current_component);
            }
        }
        private void listBoxOfModes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                ListBox box = sender as ListBox;
                current_component.Modes.Remove((string)box.SelectedItem);
                updateTablesInPropertiesTree(current_component);
            }
        }
        private void addModeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                current_component.Modes.Add(addModeTextBox.Text);
                addModeTextBox.Text = "";
                updateTablesInPropertiesTree(current_component);
            }
        }
        private void addModeTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)e.Source).Text = "+ Добавить режим";
        }
        private void RadioButton_Modes_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.IsChecked.Value) current_component.initialMode = (string)rb.Tag;
        }
        private void radioB_controlUnDis_Click(object sender, RoutedEventArgs e)
        {
            if((sender as RadioButton).IsChecked.Value)
            {
                current_component.control = (TypeOfControl)((sender as RadioButton).Tag);
            }
        }
        private void addStateTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                current_component.States.Add(addStateTextBox.Text);
                addStateTextBox.Text = "";
                updateTablesInPropertiesTree(current_component);
            }
        }
        private void addStateTextBox_MouseLeftButtonUp_1(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                TextBox box = sender as TextBox;
                box.Text = "";
            }
        }
        private void addStateTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            ((TextBox)e.Source).Text = "+ Добавить состояние";
        }
        private void listBoxOfStates_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete)
            {
                ListBox box = sender as ListBox;
                current_component.States.Remove((string)box.SelectedItem);
            }
        }
        private void RadioButton_States_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;
            if (rb.IsChecked.Value) current_component.initialState = (string)rb.Tag;
        }
        private void Box_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            int row = Grid.GetRow(box);
            int col = Grid.GetColumn(box);
            if(Grid.GetRow(box)<= current_component.table_statesChanges.Rows.Count && Grid.GetColumn(box) <= current_component.table_statesChanges.Columns.Count)
                current_component.table_statesChanges.Rows[Grid.GetRow(box) - 1][Grid.GetColumn(box) - 1] = box.SelectedItem;
        }
        private void Box_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox box = sender as TextBox;
            double value;
            if (double.TryParse(box.Text, out value))
                current_component.table_distributions.Rows[Grid.GetRow(box) - 1][Grid.GetColumn(box) - 1] = double.Parse(box.Text);
            else MessageBox.Show("Неверный числовой формат.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        private void updateTablesInPropertiesTree(Component com)
        {
            if (com == null) return;
            updateStateChangesTable(com);
            updateDistributionsTable(com);
        }
        private void updateStateChangesTable(Component com)
        {
            StackPanel panel = new StackPanel();
            Grid grid = new Grid();
            Label label;
            ComboBox box;
            RowDefinition row;
            ColumnDefinition column;
            for (int i = 0; i < com.StatesWithoutFail.Count + 1; i++)
            {
                row = new RowDefinition();
                row.Height = new GridLength(27);
                grid.RowDefinitions.Add(row);
            }
            for (int i = 0; i < com.Modes.Count + 1; i++)
            {
                column = new ColumnDefinition();
                grid.ColumnDefinitions.Add(column);
            }
            for (int i = 0; i < com.StatesWithoutFail.Count; i++)
            {
                label = new Label();
                label.Content = com.StatesWithoutFail[i];
                label.Padding = new Thickness(0, 5, 5, 5);
                Grid.SetRow(label, i + 1);
                Grid.SetColumn(label, 0);
                grid.Children.Add(label);
            }
            for (int i = 0; i < com.Modes.Count; i++)
            {
                label = new Label();
                label.Content = com.Modes[i];
                label.Padding = new Thickness(0, 5, 5, 5);
                Grid.SetRow(label, 0);
                Grid.SetColumn(label, i + 1);
                grid.Children.Add(label);
            }
            for (int i = 0; i < com.StatesWithoutFail.Count; i++)
            {
                for (int j = 0; j < com.Modes.Count; j++)
                {
                    box = new ComboBox();
                    box.ItemsSource = com.States;
                    box.SelectedIndex = 0;
                    box.IsEditable = true;
                    box.IsReadOnly = true;
                    Grid.SetColumn(box, j + 1);
                    Grid.SetRow(box, i + 1);
                    box.SelectedItem = com.table_statesChanges.Rows[i][j];
                    box.SelectionChanged += Box_SelectionChanged;
                    grid.Children.Add(box);
                }
            }
            grid.Margin = new Thickness(-17, 0, 0, 0);
            panel.Children.Add(grid);
            treeItemStatesChangesTable.Items.Clear();
            treeItemStatesChangesTable.Items.Add(panel);
        }
        private void updateDistributionsTable(Component com)
        {
            StackPanel panel = new StackPanel();
            Grid grid = new Grid();
            Label label;
            TextBox box;
            RowDefinition row;
            ColumnDefinition column;
            for (int i = 0; i < com.StatesWithoutFail.Count + 1; i++)
            {
                row = new RowDefinition();
                row.Height = new GridLength(27);
                grid.RowDefinitions.Add(row);
            }
            for (int i = 0; i < com.Modes.Count + 1; i++)
            {
                column = new ColumnDefinition();
                grid.ColumnDefinitions.Add(column);
            }
            for (int i = 0; i < com.StatesWithoutFail.Count; i++)
            {
                label = new Label();
                label.Content = com.StatesWithoutFail[i];
                //label.Padding = new Thickness(0, 5, 5, 5);

                Grid.SetRow(label, i + 1);
                Grid.SetColumn(label, 0);
                grid.Children.Add(label);
            }
            for (int i = 0; i < com.Modes.Count; i++)
            {
                label = new Label();
                label.Content = com.Modes[i];
                label.Padding = new Thickness(0, 5, 5, 5);
                Grid.SetRow(label, 0);
                Grid.SetColumn(label, i + 1);
                grid.Children.Add(label);
            }
            for (int i = 0; i < com.StatesWithoutFail.Count; i++)
            {
                for (int j = 0; j < com.Modes.Count; j++)
                {
                    box = new TextBox();
                    box.MinWidth = 40;
                    box.Margin = new Thickness(0);
                    Grid.SetColumn(box, j + 1);
                    Grid.SetRow(box, i + 1);
                    box.Text = Convert.ToString(com.table_distributions.Rows[i][j]);
                    box.TextChanged += Box_TextChanged;
                    grid.Children.Add(box);
                }
            }
            grid.Margin = new Thickness(-17, 0, 0, 0);
            panel.Children.Add(grid);
            treeItemTableDistributions.Items.Clear();
            treeItemTableDistributions.Items.Add(panel);
        }
        //Handler for the panel "Switch properties"
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            Hyperlink link = sender as Hyperlink;
            TextBlock txtblock = link.Parent as TextBlock;
            Grid grid = txtblock.Parent as Grid;
            ComboBox comboBox = grid.Children[1] as ComboBox;
            if (comboBox.Visibility == Visibility.Collapsed)
                comboBox.Visibility = Visibility.Visible;
        }
        private void ComboBox_logical_operator_Selected(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            Grid grid = comboBox.Parent as Grid;
            Grid newGrid;
            int level;
            ItemCollection items_ = null;
            if (grid.Parent is TreeView) items_ = ((TreeView)grid.Parent).Items;
            if (grid.Parent is TreeViewItem) items_ = ((TreeViewItem)grid.Parent).Items;
            if (comboBox.SelectedIndex == -1 || comboBox.SelectedIndex == 0) return;
            items_.Insert(items_.IndexOf(grid), createLogicalLabel(((ComboBoxItem)(comboBox.SelectedItem)).Content.ToString()));
            level = getLevel(grid.Parent);
            if (grid.Parent is TreeViewItem)
                newGrid = createNewGrid(null, 5 + 12 * level, -21 - 12 * level);
            else
                newGrid = createNewGrid(null, 2, -22, 1);
            items_.Insert(items_.IndexOf(grid), newGrid);
            comboBox.SelectedIndex = 0;
            comboBox.Visibility = Visibility.Collapsed;
        }
        private void ComboBox_Selected(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            if (((Component)box.SelectedItem).Name == "Выражение")
            {
                int level;
                TreeViewItem tree_item;
                level = getLevel(((Grid)(box.Parent)).Parent);
                tree_item = createSubCondition(5 + 12 * (level + 1), -21 - 12 * (level + 1));

                ItemCollection items_ = null;
                Grid grid = box.Parent as Grid;
                if (grid.Parent is TreeView) items_ = ((TreeView)grid.Parent).Items;
                if (grid.Parent is TreeViewItem) items_ = ((TreeViewItem)grid.Parent).Items;
                items_.Insert(items_.IndexOf(grid), tree_item);
                items_.Remove(grid);
            }
            else
                if(box.Parent!=null)
                ((ComboBox)((Grid)box.Parent).Children[5]).SelectedIndex = 0;
        }
        private void ComboBox_ChooseAction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = sender as ComboBox;
            Grid grid = comboBox.Parent as Grid;
            if (grid == null) return;
            Component comp = ((ComboBox)grid.Children[3]).SelectedItem as Component;
            if (comp == null) return;
            if (comboBox.SelectedItem as string == actions[0])
            {
                ((ComboBox)grid.Children[4]).ItemsSource = null;
                ((ComboBox)grid.Children[6]).ItemsSource = null;
                ((ComboBox)grid.Children[4]).SelectedIndex = -1;
                ((ComboBox)grid.Children[6]).SelectedIndex = -1;
                ((ComboBox)grid.Children[4]).IsEnabled = false;
                ((ComboBox)grid.Children[6]).IsEnabled = false;
            }
            if (comboBox.SelectedItem as string == actions[1])
            {
                ((ComboBox)grid.Children[4]).IsEnabled = true;
                ((ComboBox)grid.Children[6]).IsEnabled = true;
                ((ComboBox)grid.Children[4]).ItemsSource = comp.StatesWithoutFail;
                ((ComboBox)grid.Children[6]).ItemsSource = comp.States;
                ((ComboBox)grid.Children[4]).SelectedIndex = 0;
                ((ComboBox)grid.Children[6]).SelectedIndex = 0;

            }
            if (comboBox.SelectedItem as string == actions[2])
            {
                ((ComboBox)grid.Children[4]).IsEnabled = true;
                ((ComboBox)grid.Children[6]).IsEnabled = true;
                ((ComboBox)grid.Children[4]).ItemsSource = comp.Modes;
                ((ComboBox)grid.Children[6]).ItemsSource = comp.Modes;
                ((ComboBox)grid.Children[4]).SelectedIndex = 0;
                ((ComboBox)grid.Children[6]).SelectedIndex = 0;
            }
            if (comboBox.SelectedItem as string == actions[3])
            {
                ((ComboBox)grid.Children[4]).IsEnabled = true;
                ((ComboBox)grid.Children[4]).ItemsSource = comp.States;
                ((ComboBox)grid.Children[4]).SelectedIndex = 0;
                ((ComboBox)grid.Children[6]).SelectedIndex = -1;
                ((ComboBox)grid.Children[6]).IsEnabled=false;
            }
            if (comboBox.SelectedItem as string == actions[4])
            {
                ((ComboBox)grid.Children[4]).IsEnabled = true;
                ((ComboBox)grid.Children[4]).ItemsSource = comp.Modes;
                ((ComboBox)grid.Children[4]).SelectedIndex = 0;
                ((ComboBox)grid.Children[6]).SelectedIndex = -1;
                ((ComboBox)grid.Children[6]).IsEnabled = false;
            }
        }
        private void CompomentsBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ComboBox_conditionals.SelectedIndex == -1 ) return;
            ComboBox box = sender as ComboBox;
            Component selected_comp = box.SelectedItem as Component;
            int col = Grid.GetColumn(box);
            int row = Grid.GetRow(box);
            ComboBox states_box = (ComboBox)grid_conditions.Children.Cast<UIElement>().First(el => Grid.GetRow(el) == row && Grid.GetColumn(el) == 1);
            ComboBox states_box2 = (ComboBox)grid_conditions.Children.Cast<UIElement>().First(el => Grid.GetRow(el) == row && Grid.GetColumn(el) == 3);
            if (selected_comp == null) return;
            states_box.ItemsSource = selected_comp.StatesWithoutFail;
            states_box2.ItemsSource = selected_comp.StatesWithoutFail;
        }
        private void hyplink_actions_Click(object sender, RoutedEventArgs e)
        {
            RowDefinition row = new RowDefinition();
            grid_actions.RowDefinitions.Add(row);
            int rows = grid_actions.RowDefinitions.Count - 1;
            ComboBox box;

            box = new ComboBox(); box.IsEditable = true; box.IsReadOnly = true;
            System.ComponentModel.ICollectionView filteredView = new CollectionViewSource { Source = components }.View;
            //ListCollectionView view = CollectionViewSource.GetDefaultView(box.ItemsSource) as ListCollectionView;
            filteredView.Filter = delegate (object item) {
                Component com = item as Component;
                if (com.IsWrapped || com.IsGeneral) return false;
                else return true;
            };
            box.ItemsSource = filteredView;
            box.DisplayMemberPath = "Name";
            box.SelectedIndex = -1;
            box.Margin = new Thickness(0, 0, 0, 1);
            Grid.SetColumn(box, 0);
            Grid.SetRow(box, rows);
            grid_actions.Children.Add(box);
            box.SelectionChanged += Box_AddAction_SelectionChanged;
            //add another box
            box = new ComboBox(); box.IsEditable = true; box.IsReadOnly = true;
            box.IsEnabled = false;
            box.Margin = new Thickness(0, 0, 0, 1);
            Grid.SetColumn(box, 2);
            Grid.SetRow(box, rows);
            grid_actions.Children.Add(box);
            //add another box
        }
        private void Box_AddAction_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox left_box = sender as ComboBox;
            Grid grid = left_box.Parent as Grid;
            Component comp = left_box.SelectedItem as Component;
            ComboBox right_box = (ComboBox)grid.Children.Cast<UIElement>().First(b => Grid.GetRow(left_box) == Grid.GetRow(b) && Grid.GetColumn(b)==2);
            right_box.IsEnabled = true;
            right_box.ItemsSource = comp.Modes;
        }
        private void TxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TextBox box = sender as TextBox;
                StackPanel pan = box.Parent as StackPanel;
                Label lable = new Label();
                Component c = box.Tag as Component;
                if(c!=null) c.Name = box.Text;
                lable.Content = box.Text;
                lable.Tag = box.Tag;
                pan.Children.Remove(box);
                pan.Children.Add(lable);

            }
        }
        private void txtBox_lostFocus(object sender, RoutedEventArgs e)
        {
            TextBox box = sender as TextBox;
            StackPanel pan = box.Parent as StackPanel;
            Label lable = new Label();
            lable.Content=box.Text;
            if (pan == null) return;
            if (!pan.Children.Contains(box)) return;
            pan.Children.Remove(box);
            pan.Children.Add(lable);

        }
        private int getLevel(object item)
        {
            if (item is TreeViewItem && item != null)
                return getLevel(((TreeViewItem)item).Parent) + 1;
            else return 0;

        }
        private Grid createNewGrid(object data, int leftBox_offset = 4, int grid_left_offset = -23, int rightBox_offset = 0)
        {
            Binding binding;
            ComboBox box;
            Grid grid = new Grid();
            RowDefinition row = new RowDefinition();
            grid.Background = Brushes.White;
            grid.HorizontalAlignment = HorizontalAlignment.Stretch;
            binding = new Binding();
            binding.Source = grid_conditions;
            binding.Path = new PropertyPath("ActualWidth");
            grid.SetBinding(Grid.WidthProperty, binding);
            grid.Margin = new Thickness(grid_left_offset, 0, 0, 0);
            grid.RowDefinitions.Add(row);
            row.MinHeight = 15;
            row.Height = GridLength.Auto;


            ColumnDefinition column;
            column = new ColumnDefinition();
            binding = new Binding();
            binding.Source = comp_column;
            binding.Path = new PropertyPath("Width");
            binding.Mode = BindingMode.TwoWay;
            column.SetBinding(ColumnDefinition.WidthProperty, binding);
            grid.ColumnDefinitions.Add(column);

            column = new ColumnDefinition();
            column.Width = GridLength.Auto;
            grid.ColumnDefinitions.Add(column);

            column = new ColumnDefinition();
            binding = new Binding();
            binding.Source = pre_state_column;
            binding.Path = new PropertyPath("Width");
            binding.Mode = BindingMode.TwoWay;
            column.SetBinding(ColumnDefinition.WidthProperty, binding);
            grid.ColumnDefinitions.Add(column);

            column = new ColumnDefinition();
            column.Width = GridLength.Auto;
            grid.ColumnDefinitions.Add(column);

            column = new ColumnDefinition();
            binding = new Binding();
            binding.Source = action_column;
            binding.Path = new PropertyPath("Width");
            binding.Mode = BindingMode.TwoWay;
            column.SetBinding(ColumnDefinition.WidthProperty, binding);
            grid.ColumnDefinitions.Add(column);

            column = new ColumnDefinition();
            column.Width = GridLength.Auto;
            grid.ColumnDefinitions.Add(column);

            column = new ColumnDefinition();
            binding = new Binding();
            binding.Source = next_state_column;
            binding.Path = new PropertyPath("Width");
            binding.Mode = BindingMode.TwoWay;
            column.SetBinding(ColumnDefinition.WidthProperty, binding);
            grid.ColumnDefinitions.Add(column);

            addSplitters(grid, 0);
            //add ComboBox
            //Компонент
            box = new ComboBox();
            box.IsEditable = true;
            box.IsReadOnly = true;
            Grid.SetColumn(box, 0);
            Grid.SetRow(box, 0);
            box.SelectionChanged += ComboBox_Selected;
            box.Margin = new Thickness(leftBox_offset, 0, 0, 0);
            box.ItemsSource = components;
            box.DisplayMemberPath = "Name";
            box.SelectedIndex = 0;
            grid.Children.Add(box);
            //Предыдущее состояние
            box = new ComboBox();
            box.IsEditable = true;
            box.IsReadOnly = true;
            box.IsEnabled = false;
            box.SelectedIndex = 0;
            Grid.SetColumn(box, 2);
            Grid.SetRow(box, 0);
            grid.Children.Add(box);
            //Действие
            box = new ComboBox();
            box.IsEditable = true;
            box.IsReadOnly = true; 
            Grid.SetColumn(box, 4);
            Grid.SetRow(box, 0);
            box.ItemsSource = actions;
            box.SelectedIndex = 0;
            box.SelectionChanged += ComboBox_ChooseAction_SelectionChanged;
            grid.Children.Add(box);
            //Следующее состояние
            box = new ComboBox();
            box.IsEditable = true;
            box.IsReadOnly = true;
            box.IsEnabled = false;
            box.SelectedIndex = 0;
            box.Margin = new Thickness(0, 0, rightBox_offset, 0);
            Grid.SetColumn(box, 6);
            Grid.SetRow(box, 0);
            grid.Children.Add(box);
            return grid;
        }
        private Label createLogicalLabel(string str)
        {
            Label label = new Label();
            label.HorizontalAlignment = HorizontalAlignment.Stretch;
            label.Margin = new Thickness(-21, -6, 0, -6);
            label.Content = str;
            label.FontWeight = FontWeights.Bold;
            label.Background = Brushes.Transparent;
            return label;
        }
        private void addSplitters(Grid grid, int last_row_index)
        {
            GridSplitter splitter = new GridSplitter();
            splitter.Width = 2;
            splitter.VerticalAlignment = VerticalAlignment.Stretch;
            splitter.ResizeDirection = GridResizeDirection.Columns;
            splitter.HorizontalAlignment = HorizontalAlignment.Center;
            Grid.SetColumn(splitter, 1);
            Grid.SetRow(splitter, last_row_index);
            grid.Children.Add(splitter);

            splitter = new GridSplitter();
            splitter.Width = 2;
            splitter.VerticalAlignment = VerticalAlignment.Stretch;
            splitter.ResizeDirection = GridResizeDirection.Columns;
            splitter.HorizontalAlignment = HorizontalAlignment.Center;
            Grid.SetColumn(splitter, 3);
            Grid.SetRow(splitter, last_row_index);
            grid.Children.Add(splitter);

            splitter = new GridSplitter();
            splitter.Width = 2;
            splitter.VerticalAlignment = VerticalAlignment.Stretch;
            splitter.ResizeDirection = GridResizeDirection.Columns;
            splitter.HorizontalAlignment = HorizontalAlignment.Center;
            Grid.SetColumn(splitter, 5);
            Grid.SetRow(splitter, last_row_index);
            grid.Children.Add(splitter);
        }
        private TreeViewItem createSubCondition(int leftBox_offset = 15, int grid_left_offset = -33)
        {
            TreeViewItem tree_item = new TreeViewItem();
            tree_item.Header = "Выражение";
            tree_item.Margin = new Thickness(-7, 0, 0, 0);
            tree_item.Items.Add(createNewGrid(null, leftBox_offset, grid_left_offset));
            tree_item.Items.Add(createLink());
            tree_item.IsExpanded = true;
            tree_item.HorizontalAlignment = HorizontalAlignment.Stretch;
            return tree_item;
        }
        private Grid createLink()
        {
            ComboBox box;
            TextBlock txtBlock;
            Hyperlink link;
            Grid grid = new Grid();
            RowDefinition row = new RowDefinition();
            grid.Background = Brushes.White;
            grid.HorizontalAlignment = HorizontalAlignment.Stretch;
            grid.Margin = new Thickness(-20, 0, 0, 0);
            //Creating link
            link = new Hyperlink();
            link.Click += Hyperlink_Click;
            link.Inlines.Add("Добавить условие");
            //Creating textBlock
            txtBlock = new TextBlock();
            txtBlock.Inlines.Add(link);
            //Creating Combobox
            box = new ComboBox();
            box.Width = 130;
            box.IsEditable = true;
            box.IsReadOnly = true;
            box.Margin = new Thickness(0, 3, 0, 0);
            box.BorderThickness = new Thickness(0);
            box.HorizontalAlignment = HorizontalAlignment.Left;
            box.Visibility = Visibility.Collapsed;
            ComboBoxItem item;
            item = new ComboBoxItem();
            item.Content = "Добавить условие";
            box.Items.Add(item);
            item = new ComboBoxItem();
            item.Content = "ИЛИ";
            box.Items.Add(item);
            item = new ComboBoxItem();
            item.Content = "И";
            box.Items.Add(item);
            item = new ComboBoxItem();
            item.Content = "ИЛИ-НЕ";
            box.Items.Add(item);
            item = new ComboBoxItem();
            item.Content = "И-НЕ";
            box.Items.Add(item);
            box.SelectedIndex = 0;
            box.SelectionChanged += ComboBox_logical_operator_Selected;
            box.Style =this.FindResource("comboBox_expandOnClick_style") as Style;

            grid.RowDefinitions.Add(row);
            row.MinHeight = 25;
            row.Height = GridLength.Auto;
            grid.Children.Add(txtBlock);
            grid.Children.Add(box);
            return grid;
        }
        private void addLableToTable(Grid grid, string str)
        {
            RowDefinition  row = new RowDefinition();
            grid_conditions.RowDefinitions.Add(row);
            int last_row_index = grid_conditions.RowDefinitions.Count - 1;
            Label label = new Label();
            label.HorizontalAlignment = HorizontalAlignment.Stretch;
            label.Margin = new Thickness(0, -3, 0, -3);
            label.Content = str;
            label.FontWeight = FontWeights.Bold;
            Grid.SetColumn(label, 0);
            Grid.SetRow(label, last_row_index);
            Grid.SetColumnSpan(label, 4);
            grid.Children.Add(label);
        }
        //DockablePane menu
        private void MItem_addComp_Click(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = new TreeViewItem();
            StackPanel panel = new StackPanel();
            ContextMenu menu = new ContextMenu();
            MenuItem mItem = new MenuItem();
            Image img = new Image();
            TextBox txtBox = new TextBox();
            Component newComponent;
            panel.Orientation = Orientation.Horizontal;

            mItem.Header = "Добавить компонент";
            mItem.Icon = new Image() { Source = (ImageSource)this.FindResource("imageAdd") };
            mItem.Click += MItem_addComp_Click;
            menu.Items.Add(mItem);

            img.Source = (ImageSource)Resources["imageComponent"];
            //txtBox.IsReadOnly = true;

            txtBox.BorderThickness = new Thickness(0);
            txtBox.Margin = new Thickness(3, 5, 5, 5);
            txtBox.SelectAll();
            txtBox.LostFocus += txtBox_lostFocus;
            txtBox.KeyDown += TxtBox_KeyDown;

            mItem = new MenuItem();
            mItem.Header = "Переименовать";
            mItem.Icon = new Image() { Source = (ImageSource)this.FindResource("imageRename"), Width = 16 };
            mItem.Click += MItem_Rename_Click;
            menu.Items.Add(mItem);

            mItem = new MenuItem();
            mItem.Header = "Удалить";
            mItem.Icon = new Image() { Source = (ImageSource)this.FindResource("imageDelete") };
            mItem.Click += MItem_Delete_Click;
            menu.Items.Add(mItem);

            img.Width = 16;
            img.Source = (ImageSource)this.FindResource("imageComponent");
            panel.Children.Add(img);
            panel.Children.Add(txtBox);

            item.Header = panel;
            item.ContextMenu = menu;
            newComponent = new Component(new ObservableCollection<string>(), new ObservableCollection<string>());
            txtBox.Tag = newComponent;
            item.Tag = newComponent;
            components.Add(newComponent);
            item.DataContext = ((Component)item.Tag);

            MenuItem obMenuItem = e.OriginalSource as MenuItem;
            ContextMenu menu1 = obMenuItem.Parent as ContextMenu;
            if (menu1.PlacementTarget is TreeViewItem)
            {
                (menu1.PlacementTarget as TreeViewItem).Items.Add(item);
                (menu1.PlacementTarget as TreeViewItem).IsExpanded = true;
            }
            else if (menu1.PlacementTarget is DockablePane)
            {
                tree_comp.Items.Add(item);
            }
            item.Focus();
            panel.Focus();
            txtBox.Focusable = true;
            //FocusManager.SetFocusedElement(panel, txtBox);
            txtBox.Focus();
            txtBox.SelectAll();
        }
        private void MItem_addFolder_Click(object sender, RoutedEventArgs e)
        {//Creating a folder
            TreeViewItem item = new TreeViewItem();
            StackPanel panel = new StackPanel();
            ContextMenu menu = new ContextMenu();
            MenuItem mItem = new MenuItem();
            Image img = new Image();
            TextBox txtBox = new TextBox();
            panel.Orientation = Orientation.Horizontal;

            mItem.Header = "Добавить компонент";
            mItem.Icon = new Image() { Source = (ImageSource)this.FindResource("imageAdd") };
            mItem.Click += MItem_addComp_Click;
            menu.Items.Add(mItem);
            //txtBox.IsReadOnly = true;
            txtBox.BorderThickness = new Thickness(0);
            txtBox.Text = "Новая папка";
            txtBox.Focusable = true;
            txtBox.Margin = new Thickness(3, 5, 5, 5);
            FocusManager.SetFocusedElement(panel, txtBox);
            txtBox.Focus();
            txtBox.SelectAll();
            txtBox.LostFocus += txtBox_lostFocus;
            txtBox.KeyDown += TxtBox_KeyDown;



            mItem = new MenuItem();
            mItem.Header = "Переименовать";
            mItem.Icon = new Image() { Source = (ImageSource)this.FindResource("imageRename"), Width = 16 };
            mItem.Click += MItem_Rename_Click;
            menu.Items.Add(mItem);

            mItem = new MenuItem();
            mItem.Header = "Удалить";
            mItem.Icon = new Image() { Source = (ImageSource)this.FindResource("imageDelete") };
            //Resources["imageDelete"];
            mItem.Click += MItem_Delete_Click;
            menu.Items.Add(mItem);
            img.Width = 16;
            img.Source = (ImageSource)this.FindResource("imageFolder");
            panel.Children.Add(img);
            panel.Children.Add(txtBox);

            item.Header = panel;
            item.ContextMenu = menu;
            item.Tag = TypeOfElement.Folder;

            tree_comp.Items.Add(item);


        }
        //Handler for main menu
        private void MenuItem_Save_Click(object sender, RoutedEventArgs e)
        {

        }
        private void MenuItem_Open_Click(object sender, RoutedEventArgs e)
        {

        }
        private void MenuItem_Exit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void MenuItem_Generate_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
